import * as api from 'api.js';
import * as login from './login.js';

export function post(req, res) {
    const user = req.body;

    api.post('api/users', user).then(response => {
        if (response.username && response.password)
            req.session.user = response.user;
        api.post('authentication_token', user).then(response => {
            const token = api.get(`api/users?username=${user.username}`);
            res.setHeader('Content-Type', 'application/json');
            if (token['hydra:member']) {
                req.session.user = token['hydra:member'][0];
                res.end(response);
            } else res.end(response);
        });
    });
}