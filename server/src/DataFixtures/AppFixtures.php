<?php

namespace App\DataFixtures;

use App\Entity\Album;
use App\Entity\Author;
use App\Entity\Playlist;
use App\Entity\Song;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $user = new User();
        $user->setUsername("username_non_author");
        $user->setPassword($this->passwordEncoder->encodePassword($user,'userpasswd'));
        $manager->persist($user);

        $userAuthor = new User();
        $userAuthor->setUsername("username_author");
        $userAuthor->setPassword($this->passwordEncoder->encodePassword($user,'userpasswd'));

        $author = (new Author())
            ->setName("Michel")
            ->setUser($userAuthor);

        $userAuthor->setAuthor($author);

        $manager->persist($userAuthor);
        $manager->persist($author);

        $album1 = (new Album())
            ->setName("album1")
            ->setAuthor($author);

        $album2 = (new Album())
            ->setName("album2")
            ->setAuthor($author);

        $playlist = (new Playlist())
            ->setName("playlist")
            ->setUser($user);


        for ($i = 0; $i <= 5; $i++) {
            $song = (new Song())
                ->setName("song" . $i)
                ->setAuthor($author)
                ->setAlbum($album1);
            $album1->addSong($song);
            $playlist->addSong($song);
            $manager->persist($song);
        }
        for ($i = 6; $i <= 10; $i++) {
            $song = (new Song())
                ->setName("song" . $i)
                ->setAuthor($author)
                ->setAlbum($album1);
            $album2->addSong($song);
            $playlist->addSong($song);
            $manager->persist($song);
        }

        $manager->persist($album1);
        $manager->persist($album2);
        $manager->persist($playlist);

        $manager->flush();
    }
}
