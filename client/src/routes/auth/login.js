import * as api from 'api.js';

export function post(req, res) {
	const user = req.body;
	api.post('authentication_token', user).then(response => {
		if (response.token)
			req.session.user = api.get(`api/users?username=${user.username}`)['hydra:member'][0];

		res.setHeader('Content-Type', 'application/json');
		res.end(response);
	});
}