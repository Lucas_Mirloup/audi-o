export async function parseAlbumProperties(prefetcher, album) {
    album.author = await (await prefetcher.fetch(`http://audi-o_back:8000${album['author']}`)).json();
    album.songs = await parseSongs(prefetcher, album.songs);
    if (!album.hasOwnProperty('coverUrl'))
        album.coverUrl = "https://alphafa.com/wp-content/uploads/2018/09/placeholder-square.jpg"
}

export async function parseAuthorProperties(prefetcher, author) {
    if (author.hasOwnProperty('albums')) {
        let albums = [];
        for (const album_url of author['albums']) {
            const album = await (await prefetcher.fetch(`http://audi-o_back:8000${album_url}`)).json();
            await parseAlbumProperties(prefetcher, album);
            albums.push(album);
        }
        author.albums = albums;
    }
}

export async function parsePlaylistProperties(prefetcher, playlist) {
    if (playlist.hasOwnProperty('songs')) {
        playlist.songs = await parseSongs(prefetcher, playlist.songs);
    }
    playlist.user = await (await prefetcher.fetch(`http://audi-o_back:8000${playlist['user']}`)).json();
}

export async function parseSongs(prefetcher, raw_songs) {
    const songs = [];
    for (const song_url of raw_songs) {
        const song = await (await prefetcher.fetch(`http://audi-o_back:8000${song_url}`)).json();
        await parseSongProperties(prefetcher, song);
        songs.push(song);
    }
    return songs;
}

export async function parseSongProperties(prefetcher, song) {
    song.album = await (await prefetcher.fetch(`http://audi-o_back:8000${song['album']}`)).json();
    song.author = await (await prefetcher.fetch(`http://audi-o_back:8000${song['author']}`)).json();
}

export async function getUser(username) {
    const user = (await (await fetch(`http://audi-o_back:8000/api/users?username=${username}`)).json())['hydra:member'][0];
    if (user.author) {
        user.author = await (await fetch(`http://audi-o_back:8000${user['author']}`)).json();
    }
    return user;
}
