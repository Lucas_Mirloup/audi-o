# Projet Audi'O (+ Docker)

Basic online music catalog

## Docker

Add the Docker network with

`docker network create web`

then create the containers with

`docker-compose up -d`